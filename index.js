let mysql = require('mysql');
let config = require('./config.json');
let pool  = mysql.createPool({
    host     : config.host,
    port     : config.port,
    user     : config.user,
    password : config.password,
    database : config.name
});

exports.runQuery = async function(sql, params) {
    return new Promise((resolve, reject) => {
        pool.getConnection((err, connection) => {
            connection.query(sql, params, (err, results) => {
                if (err){
                    reject(err);
                }
                connection.release();
                resolve(results);
            });
        });
    });
};

